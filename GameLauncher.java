import java.util.Scanner;	

public class GameLauncher {
	public static void main(String[] arg){
		//Runs the game launcher
		userGreeter();
	}
	
	public static void userGreeter(){
		
		//Greets the user and asks for an integer input to choose a game 
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello, What game would you like to play? (1 for hangman and 2 for wordle)");
		int userInput = reader.nextInt();
		
		gameChoice(userInput);
	}
	
	//Uses user input to decide what game to play depending and runs the matching condition
	public static void gameChoice(int userInput){
		Scanner reader = new Scanner(System.in);
		
		if(userInput == 1){
			System.out.println("Enter a 4 letter word(IN CAPS):");
			String hangmanAnswer = reader.next();
		
			//Converts hangman answer to uppercase
			hangmanAnswer = hangmanAnswer.toUpperCase();
		
			//Runs hangman game
			Hangman.runGame(hangmanAnswer);
		}
		if(userInput == 2){
			
			//Intantates a variable to generate a word from the world class
			String wordleAnswer = Wordle.generateWord();
			
			//Runs the wordle game with the chosen answer
			Wordle.runGame(wordleAnswer);
		}
		if(userInput != 1 && userInput != 2){
			System.out.println("Invalid input, please choose between 1 for hangman & 2 for wordle");
			userGreeter();
		}
	}
}
